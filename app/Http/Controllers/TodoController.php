<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todos = Todo::all();
        return response()->json(['message' => 'successful', 'data' => $todos, 200]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'title' => 'required',
        ]);
        $todo = new Todo();
        $todo->title = $request->input('title');
        $todo->description = '';
        $todo->done = 0;
        $todo->save();
        $todos = Todo::all();
        return response()->json(['message' => 'successful', 'data' => $todos, 200]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function show(Todo $todo,$id)
    {
        $todo = Todo::find($id);
        return response()->json(['message' => 'successful', 'data' => $todo, 200]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function edit(Todo $todo, $id)
    {
        $todo = Todo::find($id);
        return response()->json(['message' => 'successful', 'data' => $todo, 200]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Todo $todo, $id)
    {
        $request->validate([
            'title' => 'required',
        ]);
        $todo = Todo::find($id);
        $todo->title = $request->input('title');
        $todo->description = $request->input('description');
        $todo->save();
        $todos = Todo::all();
        return response()->json(['message' => 'successful', 'data' => $todos, 200]);
    }
    public function statusChange($id)
    {
        $todo = Todo::find($id);
        $todo->done = $todo->done == '0'? 1 : 0;
        $todo->save();
        return response()->json(['message' => 'successful', 'data' => !$todo->done, 200]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Todo $todo, $id)
    {
        $todo = Todo::find($id);
        $todo->delete();
        return response()->json(['message' => 'successful', 'data' => $todo, 200]);
    }
}
