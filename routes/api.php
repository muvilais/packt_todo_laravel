<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/home', 'TodoController@index');
Route::get('/todo/{id}', 'TodoController@show');
Route::get('/edit/{id}', 'TodoController@edit');
Route::post('/add-todo', 'TodoController@store');
Route::post('/update/{id}', 'TodoController@update');
Route::delete('/delete/{id}', 'TodoController@destroy');
Route::get('/status-change/{id}', 'TodoController@statusChange');
